package com.matko.orb.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(entities = [CompanyDetail::class], version = 2)
abstract class CompanyDatabase : RoomDatabase(){
    abstract fun CompanyDao() : CompanyDao

    companion object {
        @Volatile private var instance: CompanyDatabase? = null
        private val LOCK = Any()

        private val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE CompanyDetail ADD COLUMN 'country' TEXT")
                database.execSQL("ALTER TABLE CompanyDetail ADD COLUMN 'city' TEXT")
                database.execSQL("ALTER TABLE CompanyDetail ADD COLUMN 'address' TEXT")

            }
        }
        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }
        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            CompanyDatabase::class.java,
            "companies"
        )
            .addMigrations(MIGRATION_1_2)
            .build()
    }

}