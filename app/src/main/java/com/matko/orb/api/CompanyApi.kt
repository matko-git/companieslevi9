package com.matko.orb.api

import com.matko.orb.model.CompanyDetailWrapper
import com.matko.orb.model.CompanySimpleWrapper
import com.matko.orb.model.Industries
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url

interface CompanyApi {

    @GET("match/")
    suspend fun searchCompanySimpleByNameAndCountry(@Query("api_key") api_key: String,
                                            @Query("name") name: String,
                                            @Query("country") country: String
    ): CompanySimpleWrapper

    @GET("fetch/{orb}/")
    suspend fun fetchCompanyDetail(@Path("orb") orb: String,
                           @Query("api_key") api_key: String
    ): CompanyDetailWrapper

    @GET
    suspend fun fetchCompanyDetailV2(@Url fetchUrl: String
    ): CompanyDetailWrapper

    @GET("dictionaries/industries")
    suspend fun fetchIndustries(@Query("api_key") api_key: String
    ): List<Industries>

    @GET("search/")
    suspend fun searchCompanyByIndustry(@Query("api_key") api_key: String,
                                            @Query("limit") limit: Int,
                                            @Query("industry") industry: String
    ): CompanySimpleWrapper

    @GET("search/")
    suspend fun searchCompanies(@Query("api_key") api_key: String,
                                @Query("name") name: String,
                                @Query("country") country: String,
                                @Query("industry") industry: String
    ) : CompanySimpleWrapper
}