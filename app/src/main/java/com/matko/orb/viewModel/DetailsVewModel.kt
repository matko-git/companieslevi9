package com.matko.orb.viewModel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.matko.orb.api.CompanyApiService
import com.matko.orb.model.CompanyDatabase
import com.matko.orb.model.CompanyDetail
import kotlinx.coroutines.launch

class DetailsVewModel() : ViewModel() {

    private val companyApiService = CompanyApiService()

    val company = MutableLiveData<CompanyDetail>()
    val loadError = MutableLiveData(false)
    val loading = MutableLiveData(false)
    val isCompanyExistingInDB = MutableLiveData<Boolean>()

    fun fetchFromRemote(fetchUrl: String) {
        loading.value = true
        loadError.value = false
        viewModelScope.launch {
            kotlin.runCatching { companyApiService.fetchCompanyDetailsV2(fetchUrl) }
                .onSuccess {
                    company.value = CompanyDetail(
                        it.orbNumber,
                        it.name,
                        it.website,
                        it.phone,
                        it.email,
                        it.employees,
                        it.description,
                        it.address.country,
                        it.address.city,
                        it.address.address1
                    )
                    loadError.value = false
                    loading.value = false
                }
                .onFailure {
                    loadError.value = true
                    loading.value = false
                }
        }
    }

    fun saveCompanyLocally(context: Context, company: CompanyDetail) {
        viewModelScope.launch {
            CompanyDatabase(context).CompanyDao().insert(company)
            isCompanyExistingInDB.value = true
        }
    }

    fun deleteCompanyFromDB(context: Context, orbNumber: String) {
        viewModelScope.launch {
            CompanyDatabase(context).CompanyDao().deleteByOrbNumber(orbNumber)
            isCompanyExistingInDB.value = false
        }
    }

    fun getCompanyFromDB(context: Context, orbNum: String?) {
        viewModelScope.launch {
            val companyFromDB = orbNum?.let {
                CompanyDatabase(context).CompanyDao().getCompanyDetail(it)
            }
            companyFromDB?.let {
                company.value = it
                isCompanyExistingInDB.value = true
            }
        }
    }

    fun isCompanyExistingInDB(context: Context, orbNum: String?) {
        viewModelScope.launch {
            val companyFromDB = orbNum?.let {
                CompanyDatabase(context).CompanyDao().getCompanyDetail(it)
            }
            isCompanyExistingInDB.value = companyFromDB != null
        }
    }

}