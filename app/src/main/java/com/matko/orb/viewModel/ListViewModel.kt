package com.matko.orb.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.matko.orb.model.CompanySimple
import com.matko.orb.api.CompanyApiService
import kotlinx.coroutines.*

class ListViewModel : ViewModel() {

    private val companyApiService = CompanyApiService()

    val companies = MutableLiveData<List<CompanySimple>>()
    val loadError = MutableLiveData(false)
    val loading = MutableLiveData(false)
    var searchJob: Job? = null

    fun search(name: String, country: String, industry: String) {
        searchJob = viewModelScope.launch {
            delay(500L)
            loading.value = true
            kotlin.runCatching { companyApiService.searchCompanies(name, country, industry) }
                .onSuccess {
                    companies.value = it.results
                    loadError.value = false
                    loading.value = false
                }
                .onFailure {
                    loading.value = false
                    loadError.value = searchJob?.isCancelled != true
                }
        }
    }
}