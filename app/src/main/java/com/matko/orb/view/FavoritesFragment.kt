package com.matko.orb.view

import android.os.Bundle
import android.text.Editable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.matko.orb.R
import com.matko.orb.databinding.FragmentFavoritesBinding
import com.matko.orb.viewModel.FavoritesViewModel

class FavoritesFragment : Fragment() {

    private val viewModel: FavoritesViewModel by viewModels()
    private lateinit var binding: FragmentFavoritesBinding
    private val favoritesListAdapter = FavoritesListAdapter(arrayListOf())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentFavoritesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observeViewModel()



        binding.companyFavoritesList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = favoritesListAdapter
        }
        viewModel.fetchFromDB(requireContext())

        binding.searchByName.doAfterTextChanged { editable: Editable? ->
            viewModel.searchByName(requireContext(), "%" + editable.toString() + "%")
        }

    }

    private fun observeViewModel() {
        viewModel.companies.observe(viewLifecycleOwner, Observer { companies ->
            companies?.let {
                binding.companyFavoritesList.isVisible = it.isNotEmpty()
                binding.listEmpty.isVisible = it.isEmpty()
                if (it.isNotEmpty()) {
                    favoritesListAdapter.updateCompanyList(it)
                }
            }
        })
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
    }
}