package com.matko.orb.view

import android.content.Context
import android.location.Geocoder
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.matko.orb.R
import com.matko.orb.databinding.FragmentMapBinding
import java.lang.Exception


class MapFragment : Fragment(), OnMapReadyCallback {

    private lateinit var mMapView: MapView
    private lateinit var mGoogleMap: GoogleMap
    private lateinit var binding: FragmentMapBinding
    private var address = ""
    private var companyName = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentMapBinding.inflate(inflater, container, false)
        mMapView = binding.map
        mMapView.getMapAsync(this)
        mMapView.onCreate(savedInstanceState)
        mMapView.onResume()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            address = MapFragmentArgs.fromBundle(it).address
            companyName = MapFragmentArgs.fromBundle(it).companyName
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        googleMap.clear()
        mGoogleMap = googleMap
        mGoogleMap.mapType = GoogleMap.MAP_TYPE_NORMAL

        val companyLocation = getLatLngFromAddress(requireContext(), address)
        val cameraPosition = companyLocation?.let {
            CameraPosition.builder()
                .target(it)
                .zoom(15f)
                .bearing(0f)
                .tilt(45f)
                .build()
        }
        cameraPosition?.let {
            CameraUpdateFactory.newCameraPosition(it)
        }
            ?.let {
                mGoogleMap.animateCamera(it)
            }

        companyLocation?.let {
            MarkerOptions()
                .position(it)
                .title(companyName)
        }?.let {
            mGoogleMap.addMarker(
                it
            )
        }
    }

    private fun getLatLngFromAddress(context: Context, address: String) : LatLng?{
        val geoCoder = Geocoder(context)
        val p1: LatLng? = try {
            val addresses = geoCoder.getFromLocationName(address, 5)
            val location = addresses[0]
            LatLng(location.latitude, location.longitude)
        } catch (e: Exception){
            Toast.makeText(requireContext(), R.string.address_not_found, Toast.LENGTH_LONG).show()
            null
        }
        return p1
    }
}