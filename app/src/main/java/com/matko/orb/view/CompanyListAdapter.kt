package com.matko.orb.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.matko.orb.R
import com.matko.orb.databinding.ItemCompanyBinding
import com.matko.orb.model.CompanySimple

class CompanyListAdapter(private val companyList: ArrayList<CompanySimple>) :
    RecyclerView.Adapter<CompanyListAdapter.CompanyViewHolder>() {

    private var _binding: ItemCompanyBinding? = null
    private val binding get() = _binding!!

    fun updateCompanyList(newCompanyList: List<CompanySimple>) {
        companyList.clear()
        companyList.addAll(newCompanyList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompanyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        _binding = ItemCompanyBinding.inflate(inflater, parent, false)
        return CompanyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CompanyViewHolder, position: Int) {
        val item = holder.itemCompanyBinding
        item.fetchUrl.text = companyList[position].fetchUrl
        item.name.text = companyList[position].name
        item.city.text = companyList[position].city

        if (companyList[position].active.equals("active")) {
            item.companyStatusIcon.setImageResource(R.drawable.company_status_active)
        } else {
            item.companyStatusIcon.setImageResource(R.drawable.company_status_inactive)
        }

        item.itemLayout.setOnClickListener {
            val action = ListFragmentDirections.actionDetails()
            action.fetchUrl = item.fetchUrl.text.toString()
            Navigation.findNavController(it).navigate(action)
        }
    }

    override fun getItemCount(): Int {
        return companyList.size
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        _binding = null
    }

    class CompanyViewHolder(val itemCompanyBinding: ItemCompanyBinding) :
        RecyclerView.ViewHolder(itemCompanyBinding.root)
}