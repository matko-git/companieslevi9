package com.matko.orb.view


import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.matko.orb.R
import com.matko.orb.databinding.FragmentDetailsBinding
import com.matko.orb.model.CompanyDetail
import com.matko.orb.viewModel.DetailsVewModel


class DetailsFragment : Fragment() {

    private val viewModel: DetailsVewModel by viewModels()
    private lateinit var binding: FragmentDetailsBinding

    private var fetchUrl = ""
    private var orbFromDB = ""
    private var company: CompanyDetail? = null
    private var loadedFromDB = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            fetchUrl = DetailsFragmentArgs.fromBundle(it).fetchUrl
            orbFromDB = DetailsFragmentArgs.fromBundle(it).orbNumber
        }

//        mMapView = binding.map
//        mMapView?.getMapAsync(this)

        observeViewModel()

        if (orbFromDB != "0") {
            loadedFromDB = true
            viewModel.getCompanyFromDB(requireContext(), orbFromDB)
        } else {
            loadedFromDB = false
            viewModel.fetchFromRemote(fetchUrl)
        }

        binding.refreshLayoutDetails.setOnRefreshListener {
            if (loadedFromDB) {
                viewModel.getCompanyFromDB(requireContext(), orbFromDB)
            } else {
                viewModel.fetchFromRemote(fetchUrl)
            }
            binding.errorMessage.isVisible = false
            binding.refreshLayoutDetails.isRefreshing = false
        }

        binding.addressLayout.setOnClickListener { addressLayout ->
            company?.let {
                var address = it.address
                address?.let { addr ->
                    if (!it.city.isNullOrEmpty()){
                        address = address + ", " + it.city
                    }
                    val action = DetailsFragmentDirections.actionDetailsFragmentToMapFragment()
                    action.address = addr

                    it.name?.let { name ->
                        action.companyName = name
                    }
                    Navigation.findNavController(addressLayout).navigate(action)
                }
            }

//                    val gmmIntentUri = Uri.parse("geo:0,0?q=" + Uri.encode(address))
//                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
//                    mapIntent.setPackage("com.google.android.apps.maps")
//                    startActivity(mapIntent)

        }

    }

    private fun observeViewModel() {
        viewModel.company.observe(viewLifecycleOwner) { company ->
            company?.let {
                with(binding) {
                    errorMessage.visibility = View.GONE
                    companyName.text = it.name
                    country.text = it.country
                    city.text = it.city
                    address.text = it.address
                    website.text = it.website
                    phone.text = it.phone
                    email.text = it.email
                    employees.text = it.employees.toString()
                    description.text = it.description
                }
                this.company = company
                if (!loadedFromDB)
                    viewModel.isCompanyExistingInDB(requireContext(), it.orbNumber)
            }
        }

        viewModel.loadError.observe(viewLifecycleOwner) { isError ->
            isError?.let {
                binding.errorMessage.isVisible = it
                binding.dataLayout.isVisible = !it
            }
        }

        viewModel.loading.observe(viewLifecycleOwner) { isLoading ->
            isLoading?.let {
                binding.loadingViewDetails.isVisible = it
                if (it) binding.errorMessage.isVisible = false
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.favorites_menu, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val iconSave = menu.findItem(R.id.action_save)
        val iconDelete = menu.findItem(R.id.action_delete)
        viewModel.isCompanyExistingInDB.observe(viewLifecycleOwner) { isExisting ->
            iconSave.isVisible = !isExisting
            iconDelete.isVisible = isExisting
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_save -> {
                company?.let { it ->
                    viewModel.saveCompanyLocally(requireContext(), it)
                    Toast.makeText(requireContext(), R.string.saved_in_db, Toast.LENGTH_SHORT).show()
                }
                true
            }
            R.id.action_delete -> {
                company?.let { it ->
                    it.orbNumber?.let { it1 -> viewModel.deleteCompanyFromDB(requireContext(), it1) }
                    Toast.makeText(requireContext(), R.string.deleted_from_db, Toast.LENGTH_SHORT).show()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}