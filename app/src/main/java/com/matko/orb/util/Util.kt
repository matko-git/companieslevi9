package com.matko.orb.util

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment

fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

object Constants{
    const val BASE_URL = "https://api.orb-intelligence.com/3/"
    const val API_KEY = "c66c5dad-395c-4ec6-afdf-7b78eb94166a"
    const val SELECT_INDUSTRY = "Select industry"
    const val SHARED_PREF_FILE = "filters"
    const val FILTER_COUNTRY = "com_matko_orb_country"
    const val FILTER_INDUSTRY = "com_matko_orb_industry"


}
